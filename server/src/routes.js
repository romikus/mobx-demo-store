const {routes, get, post, use} = require('utils/routeUtils')
const {authJwtMiddleware, authUserMiddleware} = require('components/user/auth')
const {parseBody, handleValidationError} = require('./middlewares')
module.exports = {routes}

const laptop = require('components/laptop/actions')
get('api/v1/laptops/catalog', laptop.catalog)

use(parseBody, handleValidationError, () => {
  user = require('components/user/actions')
  post('api/v1/current_user/log_in', user.logIn)
  post('api/v1/current_user/sign_up', user.signUp)

  use(authJwtMiddleware, authUserMiddleware, () => {
    get('api/v1/current_user', user.current)
  })
})

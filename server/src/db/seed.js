const db = require('../src/db')

const main = async () => {
  const count = await db.value('SELECT count(*) FROM laptops')
  if (count === 0)
    seed()
  else
    db.close()
}
main()

function seed() {
  const fs = require('fs')
  const path = require('path')
  db.setupModels()
  fs.readFile(path.join(__filename, '..', 'data.csv'), 'utf8', async (err, data) => {
    const rows = data.split('\n')
    const len = rows.length - 1

    const brands = []
    const types = []
    const cpus = []
    const gpus = []
    const systems = []
    const laptops = []

    const getId = (arr, item) => {
      let i = arr.indexOf(item)
      if (i !== -1) return i + 1
      i = arr.length
      arr[i] = item
      return i + 1
    }

    const getSpace = (space, rgx) => {
      const size = space.match(rgx)
      let amount = +size[1]
      const factor = size[2]
      if (factor === 'TB')
        amount *= 1024
      return amount
    }

    for (let i = 1; i < len; i++) {
      const row = rows[i].split(',')
      let [
        id,
        brand,
        name,
        type,
        inches,
        screen,
        cpu,
        ram,
        space,
        gpu,
        os,
        weight,
        price,
      ] = row

      const brandId = getId(brands, brand)
      if (type.indexOf('6') !== -1) continue
      const typeId = getId(types, type)
      inches = +inches
      let resolution = screen.match(/\d+x\d+/)
      if (!resolution) continue
      resolution = resolution[0]

      const cpuId = getId(cpus, cpu)
      ram = parseFloat(ram)

      const hddI = space.indexOf('HDD')
      const ssdI = space.indexOf('SSD')
      const flashI = space.indexOf('Flash')
      const hybridI = space.indexOf('Hybrid')

      let hdd, ssd, flash, hybrid
      if (hddI !== -1) hdd = getSpace(space, /([\d\.]+)(\w+) HDD/)
      if (ssdI !== -1) ssd = getSpace(space, /([\d\.]+)(\w+) SSD/)
      if (flashI !== -1) flash = getSpace(space, /([\d\.]+)(\w+) Flash/)
      if (hybridI !== -1) hybrid = getSpace(space, /([\d\.]+)(\w+) Hybrid/)

      const gpuId = getId(gpus, gpu)
      const osId = getId(systems, os)

      weight = parseFloat(weight)
      price = parseFloat(price)

      laptops.push({
        laptopTypeId: typeId,
        systemId: osId,
        name,
        brandId,
        inches,
        screen,
        resolution,
        cpuId,
        ram,
        hdd,
        ssd,
        flash,
        hybrid,
        gpuId,
        weight,
        price
      })
    }

    if (!await db.brands.exists())
      await db.brands.create(brands.map(name => ({name})))

    if (!await db.laptopTypes.exists())
      await db.laptopTypes.create(types.map(name => ({name})))

    if (!await db.cpus.exists())
      await db.cpus.create(cpus.map(cpu => ({
        name: cpu,
        ghz: +cpu.match(/([\d\.]+)GHz/)[1]
      })))

    if (!await db.gpus.exists())
      await db.gpus.create(gpus.map(name => ({name})))

    if (!await db.systems.exists())
      await db.systems.create(systems.map(name => ({name})))

    if (!await db.laptops.exists())
      await db.laptops.create(laptops)

    await db.close()
  })
}

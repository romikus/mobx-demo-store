const {Adapter} = require('pg-adapter')
const {models} = require('porm')

const setupModels = () => {
  models(db, {
    brands: require('components/brand/model'),
    cpus: require('components/cpu/model'),
    gpus: require('components/gpu/model'),
    laptops: require('components/laptop/model'),
    laptopTypes: require('components/laptopType/model'),
    systems: require('components/system/model'),
    users: require('components/user/model')
  })
}

let db
if (process.env.DATABASE_URL) {
  db = Adapter.fromURL(process.env.DATABASE_URL)
} else {
  let config
  try {
    config = require('config/database')[process.env.NODE_ENV]
    if (!config) throw new Error()
  } catch (err) {
    console.log(err)
    throw new Error('can not read database config')
  }

  db = new Adapter(config)
}

if (process.env.NODE_ENV === 'development')
  db.setupModels = setupModels
else
  db.setupModels = () => {
    db.setupModels = () => {}
    setupModels()
  }

db.filePath = __filename
module.exports = db

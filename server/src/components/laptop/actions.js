const db = require('db')

exports.catalog = async (req, res) => {
  return await db.laptops.catalog()
}

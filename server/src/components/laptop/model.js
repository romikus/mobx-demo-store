const {model} = require('porm')

module.exports = model('laptops', {
  prepared: (db) => ({
    catalog: db.base.select({
      brands: db.brands.all(),
      cpus: db.cpus.all(),
      gpus: db.gpus.all(),
      laptops: db.laptops.all(),
      laptopTypes: db.laptopTypes.all(),
      systems: db.systems.all(),
    }).take().json()
  })
})

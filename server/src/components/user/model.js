const {model} = require('porm')

module.exports = model('users', {
  findByName(name) {
    return this.where({name}).take()
  },

  nameExists({name}) {
    return this.where({name}).exists()
  }
})

const Joi = require('@hapi/joi')
const {logIn, signUp} = require('./auth')
const db = require('db')

const logUpForm = Joi.object({
  name: Joi.string().alphanum().min(3).max(30).required(),
  password: Joi.string().min(8).required(),
})

exports.logIn = async (req, res) => {
  const {value, error} = logUpForm.validate(req.body)
  if (error) throw error

  const user = await logIn(value)

  if (user)
    return JSON.stringify(user)

  const err = new Error('Name or password does not match')
  err.statusCode = 422
  throw err
}

exports.signUp = async (req, res) => {
  const {value, error} = logUpForm.validate(req.body)
  if (error) throw error

  const user = await signUp(value)
  if (user)
    return JSON.stringify(user)

  const err = new Error('Name is taken')
  err.statusCode = 422
  throw err
}

exports.current = async (req, res) =>
  JSON.stringify(req.user)

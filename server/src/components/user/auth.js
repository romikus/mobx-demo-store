const {verify} = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const db = require('db')
const secret = 'top secret'

exports.authJwtMiddleware = (req, res, next) => {
  const header = req.headers.authorization
  let data
  if (header)
    data = getUserInfoFromJWT(header)

  if (!data) {
    const error = new Error('Unauthorized')
    error.statusCode = 403
    throw error
  }

  req.userId = data.userId
  return next()
}

exports.authUserMiddleware = async (req, res, next) => {
  const id = req.userId
  if (id)
    req.user = await db.users.find(id)

  if (!req.user) {
    const error = new Error('Unauthorized')
    error.statusCode = 403
    throw error
  }

  delete req.user.password
  return next()
}

exports.logIn = async ({name, password}) => {
  const user = await db.users.findByName(name)
  let match
  if (user && password)
    match = await bcrypt.compare(password, user.password)

  if (!user || !match)
    return null

  return patchUser(user)
}

exports.signUp = async ({name, password}) => {
  if (await db.users.nameExists({name}))
    return null

  const encrypted = await bcrypt.hash(password, 10)
  const user = await db.users.create({name, password: encrypted})
  return patchUser(user)
}

function getUserInfoFromJWT(header) {
  if (header.slice(0, 7) !== 'Bearer ')
    return

  const token = header.slice(7)
  let decoded
  verify(token, secret, (error, data) => {
    if (!error)
      decoded = data
  })
  return decoded
}

function patchUser(user) {
  delete user.password
  user.token = `Bearer ${jwt.sign({userId: user.id}, secret)}`
  return user
}

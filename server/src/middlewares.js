const bodyParser = require('body-parser')

const jsonParser = bodyParser.json()
exports.parseBody = (req, res, next) => new Promise((resolve, reject) => {
  try {
    jsonParser(req, res, async () => {
      try {
        resolve(await next())
      } catch (err) {
        reject(err)
      }
    })
  } catch (err) {
    reject(err)
  }
})

exports.handleValidationError = async (req, res, next) => {
  try {
    return await next()
  } catch (err) {
    if (err.name === 'ValidationError') {
      res.statusCode = 422
      return JSON.stringify({error: err.message})
    } else if (err.statusCode) {
      res.statusCode = err.statusCode
      return JSON.stringify({error: err.message})
    } else {
      throw err
    }
  }
}

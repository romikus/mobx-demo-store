const {routes} = require('../routes')
const match = require('route-maker/match')

if (process.env.NODE_ENV === 'development') {
  const {cache} = require
  exports.clearCache = (except) => {
    for (let key in cache)
      if (!except.includes(key) && key.indexOf('/node_modules/') === -1)
        delete cache[key]
  }
} else {
  exports.clearCache = () => {}
}

let time

exports.logRequest = (req) => {
  time = Date.now()
  console.log(`\n${req.method} ${req.url}`)
}

exports.logResult = (res) => {
  console.log(`Respond with code ${res.statusCode} in ${Date.now() - time}ms`)
}

const static = require('node-static');
const fileServer = new static.Server(require('path').join(__dirname, '..', '..', '..', 'client', 'build'))
exports.processRequest = async (req, res) => {
  setupCors(req, res)

  if (req.method === 'OPTIONS')
    return respondToOptions(req, res)

  const route = findRouteAndSetParams(req)
  if (!route)
    return fileServer.serve(req, res)

  let result
  try {
    result = await route(req, res)
  } catch (err) {
    console.error(err)
    res.statusCode = 500
    return res.end()
  }

  if (result) {
    if (!res.getHeader('Content-Type'))
      res.setHeader('Content-Type', 'application/json')
    return res.end(result)
  }

  return res.end()
}

const findRouteAndSetParams = (req) => {
  const methodRoutes = routes[req.method]
  const path = req.url
  if (methodRoutes) {
    for (let routePath in methodRoutes) {
      const params = match(routePath, path)
      if (params) {
        req.params = params
        return methodRoutes[routePath]
      }
    }
  }
}

const setupCors = (req, res) => {
  res.setHeader('Access-Control-Allow-Origin', req.headers.origin || req.headers.host)
  res.setHeader('Access-Control-Allow-Credentials', 'true')
}

const respondToOptions = (req, res) => {
  res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE')
  res.setHeader('Access-Control-Allow-Headers', 'Authorization, Origin, X-Requested-With, Accept, Content-type')
  res.end()
}

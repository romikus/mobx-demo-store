const routes = { GET: {}, POST: {} }

let currentMiddlewares = []

const unboundNext = (req, res, next, middlewares, handler) => {
  const middleware = middlewares[next.num++]
  if (!middleware)
    return handler(req, res)

  return middleware(req, res, next.resolve)
}

const addRoute = (store, path, handler) => {
  const middlewares = currentMiddlewares
  store[path] = (req, res) => {
    const next = { num: 0 }
    next.resolve = unboundNext.bind(null, req, res, next, middlewares, handler)
    return next.resolve()
  }
}

module.exports = {
  routes,
  get: (path, handler) => {
    addRoute(routes.GET, path, handler)
  },
  post: (path, handler) => {
    addRoute(routes.POST, path, handler)
  },
  use: (...args) => {
    const middlewares = args.slice(0, -1)
    const fn = args[args.length - 1]

    const prevMiddlewares = currentMiddlewares
    currentMiddlewares = currentMiddlewares.concat(middlewares)
    fn()
    currentMiddlewares = prevMiddlewares
  }
}

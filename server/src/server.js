const { createServer } = require('http')
const { clearCache, logRequest, logResult } = require('./utils/serverRequest')

startServer(process.env.PORT || 8000)

function startServer(port) {
  const db = require('db')
  const persistentPaths = db.filePath
  createServer(
    handleRequest.bind(null, db, persistentPaths)
  ).listen(port, () =>
    process.stdout.write(`Running on :${port}\n`),
  )
}

function handleRequest(db, persistentPaths, req, res) {
  clearCache(persistentPaths)
  db.setupModels()
  try {
    logRequest(req)
    require('./utils/serverRequest').processRequest(req, res)
  } catch (err) {
    res.statusCode = 500
    res.end('Error 500')
    console.error(err)
  }
  logResult(res)
}

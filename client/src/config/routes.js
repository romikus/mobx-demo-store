import routeMaker from 'route-maker'

let origin
if (process.env.NODE_ENV === 'development')
  origin = process.env.REACT_APP_API_URL

const route = routeMaker.config({url: origin})
const api = route.config({prefix: 'api/v1'})

export const routes = {
  emailTaken: api('email_taken'),
  currentUser: {
    get: api('current_user'),
    logIn: api('current_user/log_in'),
    signUp: api('current_user/sign_up'),
    logOut: api('current_user/log_out')
  },
  laptops: {
    catalog: api('laptops/catalog')
  }
}

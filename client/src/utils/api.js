export const publicFetch = async (url, options = {}, data) => {
  if (!options.headers) options.headers = {}
  if (!options.headers['Content-Type']) options.headers['Content-Type'] = 'application/json;charset=utf-8'
  if (!options.credentials) options.credentials = 'include'
  if (!options.mode) options.mode = 'cors'
  if (data) options.body = JSON.stringify(data)
  const response = await fetch(url, options)
  const json = await response.json()
  return json
}

export const publicGet = (url, options = {}) => {
  options.method = 'GET'
  return publicFetch(url, options)
}

export const publicPost = (url, data, options = {}) => {
  options.method = 'POST'
  return publicFetch(url, options, data)
}

export const get = (url, options = {}) => {
  options.method = 'GET'
  if (!options.headers) options.headers = {}
  const token = localStorage.getItem('userToken')
  if (!token) throw new Error('unauthorized')
  options.headers.Authorization = token
  return publicFetch(url, options)
}

import {observable, toJS} from 'mobx'

export default class Form {
  @observable data
  @observable errors = {}

  constructor({initialValues, validations, onSubmit}) {
    this.data = initialValues
    this.validations = validations
    this.onSubmit = onSubmit
  }

  setValue = ({target: {name, value}}) => {
    this.data[name] = value
    delete this.errors[name]
  }

  validate = ({target: {name, value}}) => {
    this.setValue({target: {name, value}})
    const validation = this.validations[name]
    if (!validation) return
    try {
      validation.validateSync(this.data[name])
      this.errors[name] = null
    } catch (err) {
      this.errors[name] = err.message
    }
  }

  submit = async (e) => {
    e.preventDefault()
    const {data, errors} = this
    for (let name in data)
      if (!errors.hasOwnProperty(name))
        this.validate({target: {name, value: data[name]}})

    for (let name in errors)
      if (errors[name])
        return

    this.onSubmit(toJS(this.data))
  }
}

import {routes} from 'config/routes'
import {publicPost, get} from 'utils/api'
import AppState from 'components/App/State'
import UserStore from 'components/User/Store'

const logInOrUp = async (url, {name, password}) => {
  const user = await publicPost(url, {name, password})
  if (user.error) {
    AppState.setAuthError(user.error)
  } else {
    AppState.closeAuthDialog()
    UserStore.setCurrentUser(user)
    localStorage.setItem('userToken', user.token)
  }
}

export const logIn = logInOrUp.bind(null, routes.currentUser.logIn())
export const signUp = logInOrUp.bind(null, routes.currentUser.signUp())

export const logOut = () => {
  UserStore.setCurrentUser(null)
  localStorage.removeItem('userToken')
}

export const getCurrentUser = async () => {
  if (!localStorage.getItem('userToken')) return
  const user = await get(routes.currentUser.get())
  if (!user) return
  UserStore.setCurrentUser(user)
}

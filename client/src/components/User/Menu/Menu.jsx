import React from 'react'
import UserStore from 'components/User/Store'
import {observer} from 'mobx-react'
import UserMenu from './UserMenu'
import GuestMenu from './GuestMenu'

export default observer(() =>
  <React.Fragment>
    {UserStore.currentUser ? <UserMenu/> : <GuestMenu/>}
  </React.Fragment>
)

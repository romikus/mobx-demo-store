import React from 'react'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import {observer} from 'mobx-react'
import UserStore from 'components/User/Store'
import {logOut} from 'components/User/actions'

export default observer(() =>
  <React.Fragment>
    <Box m={1}>
      <Typography>Hello, {UserStore.currentUser.name}!</Typography>
    </Box>
    <Box m={1}>
      <Button onClick={logOut} variant='contained'>Log Out</Button>
    </Box>
  </React.Fragment>
)

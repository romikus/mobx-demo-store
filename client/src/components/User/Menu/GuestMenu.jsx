import React from 'react'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import AppState from 'components/App/State'

export default () =>
  <React.Fragment>
    <Box m={1}>
      <Button onClick={AppState.openLogInDialog} variant='contained'>Log In</Button>
    </Box>
    <Box m={1}>
      <Button onClick={AppState.openSignUpDialog} variant='contained'>Sign Up</Button>
    </Box>
  </React.Fragment>

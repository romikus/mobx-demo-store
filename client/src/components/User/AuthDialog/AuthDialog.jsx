import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import Box from '@material-ui/core/Box'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import FormLabel from '@material-ui/core/FormLabel'
import {observer} from 'mobx-react'
import {string} from 'yup'
import {logIn, signUp} from 'components/User/actions'
import AppState from 'components/App/State'

import Form from 'utils/form'

const form = new Form({
  initialValues: {
    name: '',
    password: '',
  },
  validations: {
    name: string().min(3).required(),
    password: string().min(8).required(),
  },
  onSubmit: (params) =>
    AppState.isLogIn() ? logIn(params) : signUp(params)
})

export default observer(() => {
  const {submit, setValue, validate, errors} = form
  const text = AppState.isLogIn() ? 'Log In' : 'Sign Up'
  const error = AppState.authError
  return <Dialog
    open={!!AppState.authDialogMode}
    onClose={AppState.closeAuthDialog}
    autoDetectWindowHeight={false}
    autoScrollBodyContent={false}
  >
    <form style={{width: '400px', maxWidth: '100%'}} onSubmit={submit}>
      <DialogTitle>{text}</DialogTitle>
      <DialogContent>
        <Box m={1}>
          {error && <FormLabel error={true}>{error}</FormLabel>}
        </Box>
        <Box m={1}>
          <TextField
            onChange={setValue}
            onBlur={validate}
            fullWidth
            name='name'
            type='name'
            label='Username'
            error={errors.name}
            helperText={errors.name}
          />
        </Box>
        <Box m={1}>
          <TextField
            onChange={setValue}
            onBlur={validate}
            fullWidth
            name='password'
            type='password'
            label='Password'
            error={errors.password}
            helperText={errors.password}
          />
        </Box>
      </DialogContent>
      <DialogActions>
        <Button onClick={AppState.closeAuthDialog} variant='outlined' color="primary">
          Cancel
        </Button>
        <Button type='submit' variant='outlined' color="primary">
          {text}
        </Button>
      </DialogActions>
    </form>
  </Dialog>
})

import {observable, action} from 'mobx'

class UserStore {
  @observable currentUser = null

  @action setCurrentUser(user) {
    this.currentUser = user
  }
}

export default new UserStore()

import React from 'react'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import Layout from './Layout/Layout'
import {getCurrentUser} from 'components/User/actions'
import Catalog from 'components/Laptop/Catalog/Catalog'
import Page from 'components/Page/Page'

export default () => {
  React.useEffect(() =>
    getCurrentUser()
  , [])

  return <Router>
    <Layout>
      <Switch>
        <Route path='/' exact component={Catalog} />
        <Route path='/page' component={Page} />
      </Switch>
    </Layout>
  </Router>
}

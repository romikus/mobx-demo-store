import React from 'react'
import AuthDialog from 'components/User/AuthDialog/AuthDialog'
import Header from './Header/Header'
import Main from './Main/Main'

export default ({children}) =>
  <React.Fragment>
    <Header/>
    <Main>{children}</Main>
    <AuthDialog/>
  </React.Fragment>

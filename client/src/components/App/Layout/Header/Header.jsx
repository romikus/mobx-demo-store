import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import UserMenu from 'components/User/Menu/Menu'
import {Link, useRouteMatch} from 'react-router-dom'
import cn from 'classnames'
import styles from './styles.module.scss'

const MenuLink = ({to, exact, label}) => {
  const active = useRouteMatch({path: to, exact})
  return <Link className={cn(styles.menuLink, {[styles.active]: active})} to={to}>{label}</Link>
}

export default () =>
  <AppBar className={styles.appBar}>
    <Toolbar>
      <div className={styles.mainMenu}>
        <div className={styles.title}>
          <Typography variant='h5'>MobX Demo Catalog</Typography>
        </div>
        <div className={styles.nav}>
          <MenuLink to='/' exact label='Catalog' />
          <MenuLink to='/page' label='Other Page' />
        </div>
      </div>
      <UserMenu/>
    </Toolbar>
  </AppBar>

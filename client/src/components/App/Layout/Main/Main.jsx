import React from 'react'
import style from './style.module.scss'

export default ({children}) =>
  <div className={style.main}>{children}</div>

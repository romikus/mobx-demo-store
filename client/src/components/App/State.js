import {observable, action} from 'mobx'

const authDialogStates = {
  closed: 0,
  logIn: 1,
  signUp: 2
}

class AppState {
  @observable authDialogMode = 0
  @observable authError = null

  @action closeAuthDialog = () => {
    this.authDialogMode = authDialogStates.closed
    this.authError = null
  }

  @action openLogInDialog = () =>
    this.authDialogMode = authDialogStates.logIn

  @action openSignUpDialog = () =>
    this.authDialogMode = authDialogStates.signUp

  @action setAuthError = (error) =>
    this.authError = error

  isLogIn = () =>
    this.authDialogMode === authDialogStates.logIn

  isSignUp = () =>
    this.authDialogMode === authDialogStates.signUp
}

export default new AppState()

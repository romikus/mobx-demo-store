import React from 'react'
import {loadCatalog} from 'components/Laptop/Catalog/actions'
import Catalog from 'components/Catalog/Catalog'
import {observer} from 'mobx-react'
import Store from './Store'
import GridItem from './GridItem'

export default observer(() => {
  const [catalog, setCatalog] = React.useState(null)

  React.useEffect(async () => {
    setCatalog(new Store(await loadCatalog()))
  }, [])

  return catalog && <Catalog
    products={catalog.filtered}
    sidebar={catalog.sidebarFilters}
    topBar={catalog.topElements}
    GridItem={GridItem}
  />
})

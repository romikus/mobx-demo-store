import {routes} from 'config/routes'
import {publicGet} from 'utils/api'

export const loadCatalog = async () => {
  const {laptops, brands, cpus, gpus, laptopTypes, systems} = await publicGet(routes.laptops.catalog())
  const related = {
    brand: brands,
    cpu: cpus,
    gpu: gpus,
    laptopType: laptopTypes,
    system: systems,
  }
  injectRelatedToLaptops(laptops, related)
  setDiscInfo(laptops)
  return {laptops, related}
}

function injectRelatedToLaptops(laptops, related) {
  for (let name in related) {
    const items = related[name]
    const id = `${name}Id`
    const map = {}
    items.forEach(record =>
      map[record.id] = record
    )
    laptops.forEach(laptop =>
      laptop[name] = map[laptop[id]]
    )
  }
}

function setDiscInfo(laptops) {
  const arr = []
  laptops.forEach(laptop => {
    arr.length = 0
    if (laptop.ssd) arr.push(`${laptop.ssd} SSD`)
    if (laptop.hdd) arr.push(`${laptop.hdd} HDD`)
    if (laptop.flash) arr.push(`${laptop.flash} Flash Disc`)
    if (laptop.hybrid) arr.push(`${laptop.hybrid} Hybrid`)
    laptop.disc = arr.join(' + ')
  })
}

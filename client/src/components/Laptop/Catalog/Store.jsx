import React from 'react'
import {observable, action, computed} from 'mobx'
import TextFilterState from 'components/Catalog/Components/TextFilterState'
import RangeSilderState from 'components/Catalog/Components/RangeSilderState'
import SelectMultipleState from 'components/Catalog/Components/SelectMultipleState'
import SortState from 'components/Catalog/Components/SortState'

const sortNumber = (a, b) => a < b ? -1 : 1

const collectOptions = (products, key, number) =>
  products.map(product => product[key])
    .filter((value, index, self) => self.indexOf(value) === index)
    .sort(number && sortNumber)

class LaptopCatalogStore {
  @observable laptops = []
  @observable related = []

  labels = {
    name: 'Laptop Name',
    price: 'Price',
    brand: 'Brand',
    cpu: 'Processor',
    gpu: 'Graphical Card',
    laptopType: 'Laptop Type',
    system: 'Operational System',
    resolution: 'Display Resolution',
    inches: 'Display Size (inches)',
    ram: 'RAM',
    weight: 'Weight',
    hdd: 'HDD disc',
    ssd: 'SSD disc',
    flash: 'Flash Storage',
    hybrid: 'Hybrid Storage',
  }

  constructor({laptops, related}) {
    this.related = related
    this.laptops = laptops
  }

  @computed get filters() {
    return [
      this.nameFilter,
      this.priceFilter,
      ...this.relatedFilters,
      ...this.optionsFilters,
      ...this.optionalRangesFilters,
    ]
  }

  @computed get sorting() {
    return new SortState({
      value: 'price',
      dir: 'asc',
      options: [
        {value: 'price', dir: 'asc', label: 'Price: Low to High'},
        {value: 'price', dir: 'desc', label: 'Price: High to Low'},
        {value: 'inches', dir: 'asc', label: 'Inches: Low to High'},
        {value: 'inches', dir: 'desc', label: 'Inches: High to Low'},
        {value: 'ram', dir: 'asc', label: 'Ram: Low to High'},
        {value: 'ram', dir: 'desc', label: 'Ram: High to Low'},
        {value: 'weight', dir: 'asc', label: 'Weight: Low to High'},
        {value: 'weight', dir: 'desc', label: 'Weight: High to Low'},
      ]
    })
  }

  @computed get nameFilter() {
    return new TextFilterState({
      products: this.laptops, label: this.labels.name, name: 'name', value: ''
    })
  }

  @computed get priceFilter() {
    return new RangeSilderState({
      products: this.laptops, label: this.labels.price, name: 'price'
    })
  }

  @computed get relatedFilters() {
    return Object.keys(this.related).map(name =>
      new SelectMultipleState({
        products: this.laptops,
        label: this.labels[name],
        name,
        key: `${name}Id`,
        optionLabel: (option) => option.name,
        optionValue: (option) => option.id,
        options: this.related[name]
      })
    )
  }

  @computed get optionsFilters() {
    return ['resolution', 'inches', 'ram', 'weight'].map(key =>
      new SelectMultipleState({
        products: this.laptops,
        label: this.labels[key],
        name: key,
        options: collectOptions(this.laptops, key),
      })
    )
  }

  @computed get optionalRangesFilters() {
    return ['hdd', 'ssd', 'flash', 'hybrid'].map(key =>
      new RangeSilderState({
        products: this.laptops,
        label: this.labels[key],
        name: key,
        optional: true,
        enabled: false,
      })
    )
  }

  @computed get filtered() {
    const {filters} = this
    return this.laptops.filter(laptop =>
      filters.every(filter => {
        return filter.filtered === true || filter.filtered[laptop.id]
      })
    ).sort(this.sorting.sort)
  }

  @computed get sidebarFilters() {
    return this.filters.filter(filter =>
      filter.name !== 'name'
    ).map(State =>
      <State.Component state={State} />
    )
  }

  @computed get topElements() {
    return this.filters.filter(filter =>
      filter.name === 'name'
    ).concat(this.sorting).map(State =>
      <State.Component state={State} />
    )
  }
}

export default LaptopCatalogStore

import React from 'react'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import Laptop from '@material-ui/icons/Laptop'
import LaptopChromebook from '@material-ui/icons/LaptopChromebook'
import LaptopMac from '@material-ui/icons/LaptopMac'
import LaptopWindows from '@material-ui/icons/LaptopWindows'
import {observer} from 'mobx-react'
import styles from './style.module.scss'
const images = [Laptop, LaptopChromebook, LaptopMac, LaptopWindows]

export default observer(({item}) => {
  const Image = images[item.id % 4]
  return <Card className={styles.card}>
    <CardHeader
      avatar={<Image/>}
      title={
        <React.Fragment>
          <span className={styles.inches}>{item.inches}″</span>
          <span className={styles.name}>{item.name}</span>
        </React.Fragment>
      }
      subheader={`${item.laptopType.name} from ${item.brand.name}`}
    />
    <CardContent className={styles.cardContent}>
      <div className={styles.prop}>
        <span className={styles.key}>CPU: </span>
        <span className={styles.value}>{item.cpu.name}</span>
      </div>
      <div className={styles.prop}>
        <span className={styles.key}>GPU: </span>
        <span className={styles.value}>{item.gpu.name}</span>
      </div>
      <div className={styles.prop}>
        <span className={styles.key}>OS: </span>
        <span className={styles.value}>{item.system.name}</span>
      </div>
      <div className={styles.prop}>
        <span className={styles.key}>Display: </span>
        <span className={styles.value}>{item.screen}</span>
      </div>
      <div className={styles.prop}>
        <span className={styles.key}>RAM: </span>
        <span className={styles.value}>{item.ram}</span>
      </div>
      <div className={styles.prop}>
        <span className={styles.key}>Space: </span>
        <span className={styles.value}>{item.disc}</span>
      </div>
      <div className={styles.prop}>
        <span className={styles.key}>Weight: </span>
        <span className={styles.value}>{item.weight}kg</span>
      </div>
      <div className={styles.price}>{item.price}€</div>
    </CardContent>
  </Card>
})

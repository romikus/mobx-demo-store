import {observer} from 'mobx-react'
import React from 'react'
import {FixedSizeGrid} from 'react-window'

export default observer(({products, GridItem, width, height}) => {
  const len = products.length
  if (!len) return null

  return <FixedSizeGrid
    width={width}
    height={height}
    columnWidth={width / 3}
    columnCount={3}
    rowCount={len / 3}
    rowHeight={302}
  >
    {({columnIndex, rowIndex, style}) => {
      const i = rowIndex * 3 + columnIndex
      if (i >= len) return null

      const product = products[i]
      return <div style={style}><GridItem item={product} /></div>
    }}
  </FixedSizeGrid>
})

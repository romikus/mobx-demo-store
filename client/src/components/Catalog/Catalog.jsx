import React from 'react'
import Box from '@material-ui/core/Box'
import styles from './styles.module.scss'
import Filters from './Filters'
import TopBar from './TopBar'
import {observer} from 'mobx-react'
import List from './List'

export default observer(({products, sidebar, topBar, GridItem}) => {
  const widthRef = React.useRef(null)
  const heightRef = React.useRef(null)
  const [width, setWidth] = React.useState(null)
  const [height, setHeight] = React.useState(null)

  React.useEffect(() => {
    setWidth(widthRef.current.offsetWidth - 20)
    setHeight(heightRef.current.offsetHeight - 20)
  }, [])

  return <div className={styles.root}>
    <Filters elements={sidebar} heightRef={heightRef}/>
    <Box flexGrow={1}>
      <TopBar products={products} elements={topBar} />
      <div ref={widthRef} className={styles.content}>
        {width && height &&
          <List products={products} GridItem={GridItem} width={width} height={height} />
        }
      </div>
    </Box>
  </div>
})

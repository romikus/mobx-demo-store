import React from 'react'
import Drawer from '@material-ui/core/Drawer'
import styles from './styles.module.scss'

export default ({heightRef, elements}) =>
  <Drawer
    PaperProps={{elevation: 1, ref: heightRef}}
    variant='permanent'
    classes={{paper: styles.drawerPaper}}
  >
    {elements}
  </Drawer>

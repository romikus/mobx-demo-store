import {observable} from 'mobx'
import Sort from './Sort'

export default class SortBy {
  @observable value
  @observable dir
  Component = Sort

  constructor({value, dir, options}) {
    this.value = value
    this.dir = dir
    this.options = options
  }

  sort = (a, b) => {
    const {value, dir} = this
    if (dir === 'asc')
      return a[value] > b[value] ? 1 : -1
    else
      return a[value] < b[value] ? 1 : -1
  }
}

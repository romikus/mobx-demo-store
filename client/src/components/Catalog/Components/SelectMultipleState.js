import SelectMultiple from './SelectMultiple'
import {computed, observable} from 'mobx'

const plainOption = (option) => option

export default class SelectMultipleState {
  @observable options
  Component = SelectMultiple

  constructor({
    products,
    label,
    name,
    options,
    key = name,
    optionLabel = plainOption,
    optionValue = plainOption
  }) {
    this.products = products
    this.label = label
    this.name = name
    this.key = key
    this.optionValue = optionValue
    this.options = options.map(item => ({
      label: optionLabel(item),
      value: optionValue(item),
      checked: false
    }))
  }

  @computed get selected() {
    return this.options.filter(option => option.checked)
  }

  @computed get filtered() {
    if (this.selected.length === 0)
      return true

    const ids = {}
    const {options, key} = this
    this.products.forEach(product => {
      if (options.some(option => option.checked && product[key] === option.value))
        ids[product.id] = true
    })
    return ids
  }
}

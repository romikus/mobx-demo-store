import React from 'react'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Select from '@material-ui/core/Select'
import {observer} from 'mobx-react'

export default observer(({state}) => {
  const change = (e) => {
    const option = e.target.selectedOptions[0]
    state.value = option.dataset.value
    state.dir = option.dataset.dir
  }

  const {value, dir, options} = state

  return <FormControl style={{marginLeft: 'auto'}}>
    <InputLabel>Sort By</InputLabel>
    <Select
      native
      value={`${value} ${dir}`}
      onChange={change}
    >
      {options.map(({value, dir, label}) =>
        <option data-value={value} data-dir={dir} value={`${value} ${dir}`}>
          {label}
        </option>
      )}
    </Select>
  </FormControl>
})

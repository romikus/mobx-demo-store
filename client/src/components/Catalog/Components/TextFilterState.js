import TextFilter from './TextFilter'
import {observable, computed} from 'mobx'

export default class TextFilterState {
  @observable value
  Component = TextFilter

  constructor({products, label, name, value = ''}) {
    this.products = products
    this.label = label
    this.name = name
    this.value = value
  }

  @computed get filtered() {
    const value = this.value.toLowerCase()
    if (value.length === 0)
      return true

    const ids = {}
    const {name} = this
    this.products.forEach(product => {
      if (product[name].toLowerCase().indexOf(value) !== -1)
        ids[product.id] = true
    })
    return ids
  }
}

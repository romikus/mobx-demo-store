import {observable, computed} from 'mobx'
import RangeSlider from './RangeSlider'

export default class RangeSilderState {
  @observable value
  @observable enabled
  Component = RangeSlider

  constructor({products, label, name, optional, enabled = true}) {
    this.products = products
    this.label = label
    this.name = name
    this.optional = optional
    this.enabled = enabled

    const min = this.getMinValue(name)
    const max = this.getMaxValue(name)
    this.range = [min, max]
    this.value = [min, max]
  }

  getMinValue(name) {
    return this.products.reduce((min, {[name]: current}) =>
        current < min ? current : min
      , Infinity)
  }

  getMaxValue(name) {
    return this.products.reduce((min, {[name]: current}) =>
        current > min ? current : min
      , 0)
  }

  @computed get filtered() {
    const [min, max] = this.value
    if (!this.enabled)
      return true

    const ids = {}
    const {name} = this
    this.products.forEach(product => {
      const value = product[name]
      if (value !== null && value >= min && value <= max)
        ids[product.id] = true
    })
    return ids
  }
}

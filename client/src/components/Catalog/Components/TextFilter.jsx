import React from 'react'
import {observer} from 'mobx-react'
import styles from '../styles.module.scss'
import SearchIcon from '@material-ui/icons/Search'
import InputBase from '@material-ui/core/InputBase'

export default observer(({state}) => {
  const change = (e) =>
    state.value = e.target.value

  const {label, value} = state

  return <div className={styles.search}>
    <div className={styles.searchIcon}>
      <SearchIcon />
    </div>
    <InputBase
      placeholder={label}
      value={value}
      onChange={change}
      classes={{
        root: styles.inputRoot,
        input: styles.inputInput,
      }}
      inputProps={{ 'aria-label': 'search' }}
    />
  </div>
})

import React from 'react'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import styles from '../styles.module.scss'
import Box from '@material-ui/core/Box'
import MenuItem from '@material-ui/core/MenuItem'
import Checkbox from '@material-ui/core/Checkbox'
import ListItemText from '@material-ui/core/ListItemText'
import {observer} from 'mobx-react'

const Options = observer(({state, options}) => {
  const check = (e) => {
    const value = e.target.value
    const option = state.options.find(option => String(option.value) === value)
    option.checked = e.target.checked
  }

  return <Box className={styles.box}>
    {options.map(item => (
      <MenuItem component='label' key={item.value} value={item.value}>
        <Checkbox
          value={item.value}
          checked={item.checked}
          onChange={check}
        />
        <ListItemText primary={item.label}/>
      </MenuItem>
    ))}
  </Box>
})

export default ({state, state: {label, selected, options}}) => {
  const [expanded, setExpanded] = React.useState(false)

  const expand = (e, value) =>
    setExpanded(value)

  return <ExpansionPanel expanded={expanded} onChange={expand}>
    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
      {label} {selected.length ? `(${selected.length})` : ''}
    </ExpansionPanelSummary>
    <ExpansionPanelDetails classes={{root: styles.panelDetails}}>
      {expanded &&
        <Options state={state} options={options} />
      }
    </ExpansionPanelDetails>
  </ExpansionPanel>
}

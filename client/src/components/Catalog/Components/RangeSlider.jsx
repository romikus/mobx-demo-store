import React from 'react'
import {observer} from 'mobx-react'
import Box from '@material-ui/core/Box'
import Slider from '@material-ui/core/Slider'
import Checkbox from '@material-ui/core/Checkbox'
import Typography from '@material-ui/core/Typography'
import FormControlLabel from '@material-ui/core/FormControlLabel'

export default observer(({state}) => {
  const change = (e, value) =>
    state.value = value

  const toggle = (e) =>
    state.enabled = e.target.checked

  const {
    label, optional, enabled,
    range: [min, max], value: [valueMin, valueMax]
  } = state

  return <Box m={1} mb={2}>
    {optional &&
      <FormControlLabel
        control={<Checkbox onChange={toggle} checked={enabled}/>}
        label={label}
      />
    }
    {!optional && <Typography>{label}</Typography>}
    <Slider
      disabled={!enabled}
      min={min}
      max={max}
      value={[valueMin, valueMax]}
      onChange={change}
      valueLabelDisplay="auto"
    />
  </Box>
})

import React from 'react'
import AppBar from '@material-ui/core/AppBar/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import {observer} from 'mobx-react'

export default observer(({products, elements}) =>
  <AppBar color='secodary' position='static'>
    <Toolbar>
      <Typography>{products.length} found</Typography>
      {elements}
    </Toolbar>
  </AppBar>
)
